DROP SCHEMA IF EXISTS softverski_projekti;
CREATE SCHEMA softverski_projekti DEFAULT CHARACTER SET utf8;
USE softverski_projekti;

CREATE TABLE projekat(
	id INT AUTO_INCREMENT,
	naziv VARCHAR(20) NOT NULL,
    opis VARCHAR(40) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE zadatak(
	id INT AUTO_INCREMENT,
	naziv VARCHAR(30) NOT NULL,
    username VARCHAR(20) NOT NULL,
    uradjeno DOUBLE NOT NULL,
    pripada INT NOT NULL,
	PRIMARY KEY (id),
	
	FOREIGN KEY (pripada) REFERENCES projekat(id)
	    ON DELETE RESTRICT
);


INSERT INTO projekat (id, naziv, opis) VALUES (1, 'Domaci 1', 'Domaci zadatak cas 1');
INSERT INTO projekat (id, naziv, opis) VALUES (2, 'Domaci 2', 'Domaci zadatak cas 2');
INSERT INTO projekat (id, naziv, opis) VALUES (3, 'Domaci 3', 'Domaci zadatak cas 3');
INSERT INTO projekat (id, naziv, opis) VALUES (4, 'Domaci 4', 'Domaci zadatak cas 4');
INSERT INTO projekat (id, naziv, opis) VALUES (5, 'Domaci 5', 'Domaci zadatak cas 5');

INSERT INTO zadatak (id, naziv, username, uradjeno, pripada) VALUES (1, 'Zadatak 1', 'zad1', 40.00, 5);
INSERT INTO zadatak (id, naziv, username, uradjeno, pripada) VALUES (2, 'Zadatak 2', 'zad2', 77.00, 1);
INSERT INTO zadatak (id, naziv, username, uradjeno, pripada) VALUES (3, 'Zadatak 3', 'zad3', 30.00, 1);
INSERT INTO zadatak (id, naziv, username, uradjeno, pripada) VALUES (4, 'Zadatak 4', 'zad4', 100.00, 3);
INSERT INTO zadatak (id, naziv, username, uradjeno, pripada) VALUES (5, 'Zadatak 5', 'zad5', 11.00, 4);
INSERT INTO zadatak (id, naziv, username, uradjeno, pripada) VALUES (6, 'Zadatak 6', 'zad6', 100.00, 2);
INSERT INTO zadatak (id, naziv, username, uradjeno, pripada) VALUES (7, 'Zadatak 7', 'zad7', 21.00, 1);
INSERT INTO zadatak (id, naziv, username, uradjeno, pripada) VALUES (8, 'Zadatak 8', 'zad8', 80.00, 2);

