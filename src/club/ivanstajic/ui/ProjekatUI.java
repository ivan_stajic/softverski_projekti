package club.ivanstajic.ui;

import java.util.List;

import club.ivanstajic.dao.ProjekatDAO;
import club.ivanstajic.dao.ZadatakDAO;
import club.ivanstajic.model.Projekat;
import club.ivanstajic.model.Zadatak;
import club.ivanstajic.utils.PomocnaKlasa;

public class ProjekatUI {
	
	public static void prikazSvihProjekata(List<Projekat> projekti) {
		System.out.println("\n==============================================");
		System.out.printf("%-4s %-20s %-30s\n", "ID", "NAZIV", "OPIS");
		System.out.println("==============================================");
		for (Projekat pr : projekti) {
			System.out.print(pr.toFormatedString());
		}
		System.out.println("==============================================\n");
	}
	
	public static void pronadjiProjekatZaId() {
		Projekat rez = null;
		List<Zadatak> zadatci = null;
		
		while (rez == null) {
			System.out.println("Unesite id projekta: ");
			int trazeniID = PomocnaKlasa.ocitajCeoBroj();
			rez = ProjekatDAO.getProjekatById(ApplicationUI.getConn(), trazeniID);
			zadatci = ZadatakDAO.getZadatciByPripada(ApplicationUI.getConn(), trazeniID);
		}
		System.out.println("\n==============================================");
		System.out.printf("%-4s %-20s %-30s\n", "ID", "NAZIV", "OPIS");
		System.out.println("==============================================");
		System.out.println(rez.toFormatedString());

		System.out.println("\t\tZADATCI:\n");
		System.out.println("==========================================");
		System.out.printf("%-4s %-15s %-15s %5s\n", "ID", "NAZIV", "USERNAME", "%");
		System.out.println("==========================================");
		for (Zadatak zadatak : zadatci) {
			System.out.println(zadatak.toFormatedString());
			System.out.println("------------------------------------------\n");
		}
		
	}



}
