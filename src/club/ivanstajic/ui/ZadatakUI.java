package club.ivanstajic.ui;


import club.ivanstajic.dao.ZadatakDAO;
import club.ivanstajic.model.Zadatak;
import club.ivanstajic.utils.PomocnaKlasa;

public class ZadatakUI {
	
	public static void izmenaProcentaZadatka() {
		
		ProjekatUI.pronadjiProjekatZaId();
		
		Zadatak zadatak = null;
		
		while (zadatak == null) {
			System.out.println("Unesite id zadatka: ");
			int trazeniID = PomocnaKlasa.ocitajCeoBroj();
			zadatak = ZadatakDAO.getZadatakById(ApplicationUI.getConn(), trazeniID);
		}

		if (zadatak != null) {
			System.out.print("Unesi novi procenat:");
			double procenat = PomocnaKlasa.ocitajRealanBroj();
			zadatak.setUradjeno(procenat);

			ZadatakDAO.update(ApplicationUI.getConn(), zadatak);
			System.out.println("Podatak uspesno promenjen\n");
		}
		
	}


}
