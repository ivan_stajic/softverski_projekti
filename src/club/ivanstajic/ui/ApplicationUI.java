package club.ivanstajic.ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import club.ivanstajic.dao.ProjekatDAO;
import club.ivanstajic.utils.PomocnaKlasa;

public class ApplicationUI {
	
private static Connection conn;
	
	static {
		// otvaranje konekcije, jednom na pocetku aplikacije
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");
			// otvaranje konekcije
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/softverski_projekti?useSSL=false", 
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			// kraj aplikacije
			System.exit(0);
		}
	}


	public static void main(String[] args) {

		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				ProjekatUI.prikazSvihProjekata(ProjekatDAO.getAll(conn));
				break;
			case 2:
				ProjekatUI.pronadjiProjekatZaId();
				break;
			case 3:
				ZadatakUI.izmenaProcentaZadatka();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
		
		
		// zatvaranje konekcije, jednom na kraju aplikacije
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Softverski projekti - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Prikaz svih projekata");
		System.out.println("\tOpcija broj 2 - Prikaz pojedinacnog projekta sa spiskom svih zadataka u projektu");
		System.out.println("\tOpcija broj 3 - Izmena procenta ispunjenosti zadatka za odredjeni zadatak odredjenog projekta");
		System.out.println("\tOpcija broj 4 - Unos novog projekta");
		System.out.println("\tOpcija broj 5 - Brisanje projekta");
		System.out.println("\tOpcija broj 6 - Unos novog zadatka");
		System.out.println("\tOpcija broj 7 - Brisanje zadatka");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

	public static Connection getConn() {
		return conn;
	}


}
