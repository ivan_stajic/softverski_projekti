package club.ivanstajic.model;

import java.util.ArrayList;

public class Projekat {
	
	protected int id;
	protected String naziv;
	protected String opis;
	protected ArrayList<Zadatak> listaZadataka = new ArrayList<Zadatak>();
	
	
	
	public Projekat(int id, String naziv, String opis) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
	}
	
	

	@Override
	public String toString() {
		return "Projekat [id=" + id + ", naziv=" + naziv + ", opis=" + opis + ", listaZadataka=" + listaZadataka + "]";
	}
	
	public String toFormatedString() {
		return String.format("%-4d %-20s %-30s\n", id, naziv, opis);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Projekat other = (Projekat) obj;
		if (id != other.id)
			return false;
		if (listaZadataka == null) {
			if (other.listaZadataka != null)
				return false;
		} else if (!listaZadataka.equals(other.listaZadataka))
			return false;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		if (opis == null) {
			if (other.opis != null)
				return false;
		} else if (!opis.equals(other.opis))
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public ArrayList<Zadatak> getListaZadataka() {
		return listaZadataka;
	}

	public void setListaZadataka(ArrayList<Zadatak> listaZadataka) {
		this.listaZadataka = listaZadataka;
	}
	

	
}
