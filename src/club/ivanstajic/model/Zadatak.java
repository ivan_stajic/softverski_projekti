package club.ivanstajic.model;

public class Zadatak {
	
	protected int id;
	protected String naziv;
	protected String username;
	protected double uradjeno;
	
	
	
	public Zadatak(String naziv, String username, double uradjeno) {
		super();
		this.naziv = naziv;
		this.username = username;
		this.uradjeno = uradjeno;
	}

	public Zadatak(int id, String naziv, String username, double uradjeno) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.username = username;
		this.uradjeno = uradjeno;
	}

	@Override
	public String toString() {
		return "Zadatak [id=" + id + ", naziv=" + naziv + ", username=" + username + ", uradjeno=" + uradjeno + "]";
	}
	
	public String toFormatedString() {
		return String.format("%-4d %-20s %-10s %5.2f", id, naziv, username, uradjeno);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zadatak other = (Zadatak) obj;
		if (id != other.id)
			return false;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		if (Double.doubleToLongBits(uradjeno) != Double.doubleToLongBits(other.uradjeno))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public double getUradjeno() {
		return uradjeno;
	}

	public void setUradjeno(double uradjeno) {
		this.uradjeno = uradjeno;
	}
	
}
