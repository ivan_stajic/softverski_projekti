package club.ivanstajic.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import club.ivanstajic.model.Zadatak;

public class ZadatakDAO {
	
	public static Zadatak getZadatakById(Connection conn, int id) {
		Zadatak zadatak = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT id, naziv, username, uradjeno FROM zadatak WHERE id = " + id;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int idZ = rset.getInt(index++);
				String naziv = rset.getString(index++);
				String username = rset.getString(index++);
				double uradjeno = rset.getDouble(index++);
				
				zadatak = new Zadatak(idZ, naziv, username, uradjeno);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return zadatak;
	}

	
	public static List<Zadatak> getZadatciByPripada(Connection conn, int pripada) {
		List<Zadatak> zadatci = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT id, naziv, username, uradjeno FROM zadatak WHERE pripada = " + pripada;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int idZ = rset.getInt(index++);
				String naziv = rset.getString(index++);
				String username = rset.getString(index++);
				double uradjeno = rset.getDouble(index++);
				
				Zadatak zadatak = new Zadatak(idZ, naziv, username, uradjeno);
				zadatci.add(zadatak);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return zadatci;
	}
	
	public static boolean update(Connection conn, Zadatak zadatak) {
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE zadatak SET uradjeno = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setDouble(index++, zadatak.getUradjeno());

			pstmt.setInt(index++, zadatak.getId());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}



}
