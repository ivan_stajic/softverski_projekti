package club.ivanstajic.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import club.ivanstajic.model.Projekat;

public class ProjekatDAO {
	
	public static List<Projekat> getAll(Connection conn) {
		ArrayList<Projekat> projekti = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT * FROM projekat";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				String opis = rset.getString(index++);
				
				Projekat projekat = new Projekat(id, naziv, opis);
				projekti.add(projekat);
				
			}

			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return projekti;
	}
	
	public static Projekat getProjekatById(Connection conn, int id) {
		Projekat projekat = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT id, naziv, opis FROM projekat WHERE id = " + id;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int idP = rset.getInt(index++);
				String naziv = rset.getString(index++);
				String opis = rset.getString(index++);
				
				projekat = new Projekat(idP, naziv, opis);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return projekat;
	}
	
	
//	public static Projekat getProjekatByIdWithZadaci(Connection conn, int id) {
//		Projekat projekat = null;
//		ArrayList<Zadatak> zadatci = new ArrayList<Zadatak>(); 
//		
//		Statement stmt = null;
//		ResultSet rset = null;
//		
//		try {
//			String query = "SELECT projekat.id, projekat.naziv, projekat.opis, zadatak.naziv, zadatak.username, zadatak.uradjeno FROM projekat JOIN zadatak ON projekat.id = " + id + " AND zadatak.pripada = " + id;
//			stmt = conn.createStatement();
//			rset = stmt.executeQuery(query);
//			
//			while(rset.next()) {
//				int index = 1;
//				int idP = rset.getInt(index++);
//				String naziv = rset.getString(index++);
//				String opis = rset.getString(index++);
//				String nazivZ = rset.getString(index++);
//				String username = rset.getString(index++);
//				double uradjeno = rset.getDouble(index++);
//				
//				Projekat.listaZadataka.add();
//				
//				projekat = new Projekat(idP, naziv, opis, zadatci);
//				
//			}
//		} catch (SQLException ex) {
//			System.out.println("Greska u SQL upitu!");
//			ex.printStackTrace();
//		} finally {
//			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
//			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
//		}
//		
//		return projekat;
//	}



}
